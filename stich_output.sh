#!/bin/bash
input_files=./out/*.png
#frame_rate=23.976024
output_file=./out.mp4

total_frames=$(ls out | tail -n 1 | cut -d "." -f 1)
duration_of_og=$(ffprobe -v quiet -of csv=p=0 -show_entries format=duration "$1")
duration_of_og_clean=$(echo $duration_of_og | cut -d '.' -f1)

total_frames_clean=$(echo "${total_frames#"${total_frames%%[!0]*}"}")
echo $total_frames_clean
echo $duration_of_og_clean
echo "Input file" $1

frame_rate=$(echo "$total_frames_clean/$duration_of_og_clean" | bc -l)
#let fps=$total_frames_clean/$duration_of_og_clean
echo -n "FPS: "
echo $frame_rate
#exit 0
ffmpeg -framerate $frame_rate -pattern_type glob -i "$input_files" -c:v libx264 -r $frame_rate -pix_fmt yuv420p $output_file
ffmpeg -i $1 -itsoffset 00:00:01 -i $output_file -shortest -c:v copy -c:a aac test_with_color.mp4