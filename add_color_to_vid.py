#!/usr/bin/python3

import sys
import cv2 
import numpy as np 

import argparse
import matplotlib.pyplot as plt

from colorization.colorizers import *

print(f"Input file: {sys.argv[1]}")
cap = cv2.VideoCapture(sys.argv[1])



colorizer_siggraph17 = siggraph17(pretrained=True).eval()
if(False):
    #colorizer_eccv16.cuda()
    colorizer_siggraph17.cuda()


frame_count = 0
output_dir = "./out/"
while (cap.isOpened()):
    frame_count = frame_count + 1
    print(f"frame {frame_count}")
    
    #pull frame
    ret, frame = cap.read()
    #frame = cv2.resize(frame, (540, 380), fx = 0, fy = 0, interpolation = cv2.INTER_CUBIC)
    #cv2.imshow('Frame', frame)
    
    
    #img = load_img(opt.img_path)
    (tens_l_orig, tens_l_rs) = preprocess_img(frame, HW=(256,256))
    if(False):
        tens_l_rs = tens_l_rs.cuda()

    # colorizer outputs 256x256 ab map
    # resize and concatenate to original L channel
    img_bw = postprocess_tens(tens_l_orig, torch.cat((0*tens_l_orig,0*tens_l_orig),dim=1))
    #out_img_eccv16 = postprocess_tens(tens_l_orig, colorizer_eccv16(tens_l_rs).cpu())
    out_img_siggraph17 = postprocess_tens(tens_l_orig, colorizer_siggraph17(tens_l_rs).cpu())

    #file_name = opt.img_path[:-4] + "_plus_color." + opt.img_path[-3:]
    #plt.imsave('%s_eccv16.png'%opt.save_prefix, out_img_eccv16)
    #plt.imsave(file_name, out_img_siggraph17)
    file_name = f"{output_dir}{frame_count:06}.png"
    #plt.imsave('%s_eccv16.png'%opt.save_prefix, out_img_eccv16)
    plt.imsave(file_name, out_img_siggraph17)
    #plt.imshow(out_img_siggraph17)
    #plt.title('Output (SIGGRAPH 17)')
    #plt.axis('off')
    #plt.show()
    cv2.imshow(f'Frame', out_img_siggraph17)
    
    
    if cv2.waitKey(25) & 0xFF == ord('q'): 
        break
    
cap.release()
cv2.destroyAllWindows()
